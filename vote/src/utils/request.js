import axios from 'axios'
import store from '@/store'
import {Toast} from 'vant';
import {getToken} from '@/utils/auth';
import errorCode from '@/utils/errorCode'
import {toLogin} from '@/api/login';
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8';
// 创建axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api的base_url
  timeout: 15000, // 请求超时时间,
  responseType:"json"
})

// request拦截器
service.interceptors.request.use(config => {
  // 是否需要设置 token
  const isToken = (config.headers || {}).isToken === false
  if (getToken() && !isToken) {
    // config.headers['Access-Control-Allow-Origin']='http://www.domain1.com';
    config.headers['Authorization'] = 'Bearer ' +getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  // get请求映射params参数
  if (config.method === 'get' && config.params) {
    let url = config.url + '?';
    for (const propName of Object.keys(config.params)) {
      const value = config.params[propName];
      var part = encodeURIComponent(propName) + "=";
      if (value !== null && typeof(value) !== "undefined") {
        if (typeof value === 'object') {
          for (const key of Object.keys(value)) {
            let params = propName + '[' + key + ']';
            var subPart = encodeURIComponent(params) + "=";
            url += subPart + encodeURIComponent(value[key]) + "&";
          }
        } else {
          url += part + encodeURIComponent(value) + "&";
        }
      }
    }
    url = url.slice(0, -1);
    config.params = {};
    config.url = url;
  }
  return config
}, error => {
  Promise.reject(error)
})
// respone拦截器
service.interceptors.response.use(
  res => {
      // 未设置状态码则默认成功状态
  const code = res.data.code || 200;
  //console.log(code)
    // 获取错误信息
    const msg = errorCode[code] || res.data.msg || errorCode['default']
    if(code === 401){
      Toast({
        message:msg,
        type:'fail'
      })
      store.dispatch('FedLogOut').then(() => {
        toLogin(window.location.href);
      })
    } else if(code === 500){
      Toast({
        message:msg,
        type:'fail'
      })
      store.dispatch('FedLogOut').then(() => {
        toLogin(window.location.href);
      })
      return Promise.reject(new Error(msg))
    } else if(code != 200){
      Toast({
        message:msg,
        type:'fail'
      })
      return Promise.reject(new Error(msg))
    }
    else{
       return  res.data;
    }
  },
  error => {
    //console.log('err' + error)
    let { message } = error;
    if (message == "Network Error") {
      message = "后端接口连接异常";
    }
    else if (message.includes("timeout")) {
      message = "系统接口请求超时";
    }
    else if (message.includes("Request failed with status code")) {
      message = "系统接口" + message.substr(message.length - 3) + "异常";
    }
    Toast({
      message:message,
      type:'fail'
    })
    return Promise.reject(error)
  }
)

export default service
