import request from '@/utils/request'
//获取投票用户信息
export function frontIndex(id) {
    return request({
        url: '/vote/voteFront/frontIndex/'+id,
        method: 'get'
    })
}
//投票验证码

export function verification(params) {
    return request({
        url: '/captcha/captcha/verification',
        method: 'get',
        params:params
    })
}


//投票动作
export function frontVoteByUser(data) {
    return request({
        url: '/vote/voteFront/voteByUser',
        method: 'post',
        data:data
    })
}
//投票详情
export function getItemDetails(itemId) {
    return request({
        url:'/vote/voteFront/frontGetItemDetails/'+itemId,
        method:'get'
    })
}


//排行榜
export function rankingList(voteId) {
    return request({
        url:'/vote/voteFront/frontRankingList/'+voteId,
        method:'get'
    })
}

//获取当前服务器时间
export function getServerTime(voteId) {
    return request({
        url: '/vote/voteFront/frontServerTime/'+voteId,
        method: 'get'
    })
}

//判断是否可投票
export function frontRankingCheck(data) {
    return request({
        url: '/vote/voteFront/frontRankingCheck',
        method: 'post',
        data:data
    })
}
