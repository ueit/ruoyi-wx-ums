import request from '@/utils/request'
// 登录方法
export function login() {
    return request({
        url: '/umsAuth/oauth/login/wechat_mp',
        method: 'get'
    })
}

export function toLogin(url) {
    console.log(url)
        localStorage.setItem("redirect",url);
     window.location=process.env.VUE_APP_BASE_HOST+process.env.VUE_APP_BASE_API+"/umsAuth/oauth/login/wechat_mp";
}

export function callBack(data) {
    return request({
        url: '/umsAuth/oauth/wechat_mp/callback',
        method: 'post',
        data:data
    })
}

// 退出方法
export function logout() {
    return request({
        url: '/auth/logout',
        method: 'delete'
    })
}

//获取ticket
export function getConfig(param) {
    return request({
        url:"/vote/voteFront/frontGetTicket",
        method:'get',
        params:param
    })
}
