import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuex from 'vuex'
import './assets/main.less';
import './assets/iconfont/iconfont.css';
import './assets/quill.css';
import {toLogin} from '@/api/login';
import {wxShare} from '@/utils/wxconfig';
import {Toast,Dialog,Button,List,Loading,RadioGroup, Radio,Form,Field,Checkbox, CheckboxGroup,Calendar,Col, Row,Image,Icon,Cell,
  CellGroup,Popup,Picker,Search,Collapse, CollapseItem,Divider,Panel,Tag,Grid, GridItem,NavBar,PullRefresh,CountDown,Tabbar,
  Overlay,TabbarItem,Tab, Tabs,Empty, Swipe, SwipeItem,Lazyload,Sticky,NoticeBar,DatetimePicker} from 'vant';

Vue.use(DatetimePicker);
Vue.use(Toast);
Vue.use(Dialog);
Vue.use(Button);
Vue.use(Row);
Vue.use(Col);
Vue.use(Cell);
Vue.use(List);
Vue.use(NavBar);
Vue.use(Icon);
Vue.use(Loading);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(Form);
Vue.use(Field);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(Calendar);
Vue.use(Image);
Vue.use(CellGroup);
Vue.use(Popup);
Vue.use(Picker);
Vue.use(Search);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Divider);
Vue.use(Panel);
Vue.use(Tag);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(PullRefresh);
Vue.use(CountDown);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Empty);
Vue.use(Overlay);
Vue.use(Swipe);
Vue.use(SwipeItem)
Vue.use(NoticeBar);
Vue.use(Lazyload, {
  lazyComponent: true,
});
Vue.use(Sticky);

Vue.config.productionTip = false;
Vue.prototype.toLogin = toLogin
Vue.prototype.$wxShare = wxShare;
Vue.use(Vuex);
Vue.filter('dataFormat', function (value, fmt) {
  let getDate = new Date(value);
  let o = {
    'M+': getDate.getMonth() + 1,
    'd+': getDate.getDate(),
    'h+': getDate.getHours(),
    'm+': getDate.getMinutes(),
    's+': getDate.getSeconds(),
    'q+': Math.floor((getDate.getMonth() + 3) / 3),
    'S': getDate.getMilliseconds()
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt;
});
new Vue({
  el:"#app",
  router,
  store,
  render: h => h(App),
});
