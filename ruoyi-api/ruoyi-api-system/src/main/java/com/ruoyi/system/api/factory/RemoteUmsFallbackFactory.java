package com.ruoyi.system.api.factory;

import cn.hutool.json.JSONObject;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.FeignUmsService;
import com.ruoyi.system.api.domain.UmsAccountAuth;
import com.ruoyi.system.api.model.ThirdAuthUser;
import feign.hystrix.FallbackFactory;
import me.zhyd.oauth.model.AuthUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 用户服务降级处理
 * 
 * @author ruoyi
 */
@Component
public class RemoteUmsFallbackFactory implements FallbackFactory<FeignUmsService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteUmsFallbackFactory.class);

    @Override
    public FeignUmsService create(Throwable throwable)
    {
        log.error("用户服务调用失败:{}", throwable.getMessage());
        return new FeignUmsService()
        {
            @Override
            public R<ThirdAuthUser> getUserInfo(AuthUser authUser)
            {
                return R.fail("获取用户失败:" + throwable.getMessage());
            }
        };
    }
}
