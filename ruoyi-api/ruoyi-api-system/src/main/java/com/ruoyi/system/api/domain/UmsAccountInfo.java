package com.ruoyi.system.api.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 会员用户信息对象 ums_account_info
 * 
 * @author ueit
 * @date 2021-03-03
 */
public class UmsAccountInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户信息id */
    private String accountId;

    /** 昵称 */
    @Excel(name = "昵称")
    private String nickname;

    /** 头像 */
    @Excel(name = "头像")
    private String avatar;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 帐号状态 */
    @Excel(name = "帐号状态")
    private String accountStatus;

    /** 密码 */
    @Excel(name = "密码")
    private String accountPassword;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCard;

    /** 生日 */
    @Excel(name = "生日")
    private String birthday;

    /** 最后登录IP */
    @Excel(name = "最后登录IP")
    private String loginIp;

    /** 最后登陆类型*/
    @Excel(name = "最后登陆类型")
    private String loginType;

    /** 最后登陆日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后登陆日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date loginDate;

    public void setAccountId(String accountId) 
    {
        this.accountId = accountId;
    }

    public String getAccountId() 
    {
        return accountId;
    }
    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getNickname() 
    {
        return nickname;
    }
    public void setAvatar(String avatar) 
    {
        this.avatar = avatar;
    }

    public String getAvatar() 
    {
        return avatar;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setAccountStatus(String accountStatus) 
    {
        this.accountStatus = accountStatus;
    }

    public String getAccountStatus() 
    {
        return accountStatus;
    }
    public void setAccountPassword(String accountPassword) 
    {
        this.accountPassword = accountPassword;
    }

    public String getAccountPassword() 
    {
        return accountPassword;
    }
    public void setSex(String sex) 
    {
        this.sex = sex;
    }

    public String getSex() 
    {
        return sex;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setBirthday(String birthday) 
    {
        this.birthday = birthday;
    }

    public String getBirthday() 
    {
        return birthday;
    }
    public void setLoginIp(String loginIp) 
    {
        this.loginIp = loginIp;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getLoginIp()
    {
        return loginIp;
    }
    public void setLoginDate(Date loginDate) 
    {
        this.loginDate = loginDate;
    }

    public Date getLoginDate() 
    {
        return loginDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("accountId", getAccountId())
            .append("nickname", getNickname())
            .append("avatar", getAvatar())
            .append("phone", getPhone())
            .append("email", getEmail())
            .append("username", getUsername())
            .append("accountStatus", getAccountStatus())
            .append("accountPassword", getAccountPassword())
            .append("sex", getSex())
            .append("idCard", getIdCard())
            .append("birthday", getBirthday())
            .append("loginIp", getLoginIp())
            .append("loginType",getLoginType())
            .append("loginDate", getLoginDate())
            .append("createTime", getCreateTime())
            .toString();
    }
}
