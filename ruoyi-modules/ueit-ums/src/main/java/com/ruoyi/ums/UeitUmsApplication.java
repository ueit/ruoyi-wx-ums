package com.ruoyi.ums;

import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringCloudApplication
public class UeitUmsApplication {
    public static void main(String[] args){
        SpringApplication.run(UeitUmsApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  会员模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
            "                 _   _                                     \n" +
                "  _   _    ___  (_) | |_           _   _   _ __ ___    ___ \n" +
                " | | | |  / _ \\ | | | __|  _____  | | | | | '_ ` _ \\  / __|\n" +
                " | |_| | |  __/ | | | |_  |_____| | |_| | | | | | | | \\__ \\\n" +
                "  \\__,_|  \\___| |_|  \\__|          \\__,_| |_| |_| |_| |___/\n" +
                "                                                           ");
    }
}
