package com.ruoyi.ums.ums.mapper;

import com.ruoyi.system.api.domain.UmsAccountInfo;

import java.util.List;

/**
 * 会员用户信息Mapper接口
 * 
 * @author ueit
 * @date 2021-03-03
 */
public interface UmsAccountInfoMapper 
{
    /**
     * 查询会员用户信息
     * 
     * @param accountId 会员用户信息ID
     * @return 会员用户信息
     */
    public UmsAccountInfo selectUmsAccountInfoById(String accountId);

    /**
     * 查询会员用户信息列表
     * 
     * @param umsAccountInfo 会员用户信息
     * @return 会员用户信息集合
     */
    public List<UmsAccountInfo> selectUmsAccountInfoList(UmsAccountInfo umsAccountInfo);

    /**
     * 新增会员用户信息
     * 
     * @param umsAccountInfo 会员用户信息
     * @return 结果
     */
    public int insertUmsAccountInfo(UmsAccountInfo umsAccountInfo);

    /**
     * 修改会员用户信息
     * 
     * @param umsAccountInfo 会员用户信息
     * @return 结果
     */
    public int updateUmsAccountInfo(UmsAccountInfo umsAccountInfo);

    /**
     * 删除会员用户信息
     * 
     * @param accountId 会员用户信息ID
     * @return 结果
     */
    public int deleteUmsAccountInfoById(String accountId);

    /**
     * 批量删除会员用户信息
     * 
     * @param accountIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteUmsAccountInfoByIds(String[] accountIds);
}
