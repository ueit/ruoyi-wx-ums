package com.ruoyi.ums.auth.service.impl;

import com.ruoyi.system.api.domain.UmsAccountAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ums.auth.mapper.UmsAccountAuthMapper;
import com.ruoyi.ums.auth.service.IUmsAccountAuthService;

import java.util.List;

/**
 * 授权信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-03-03
 */
@Service
public class UmsAccountAuthServiceImpl implements IUmsAccountAuthService 
{
    @Autowired
    private UmsAccountAuthMapper umsAccountAuthMapper;

    /**
     * 查询授权信息
     * 
     * @param id 授权信息ID
     * @return 授权信息
     */
    @Override
    public UmsAccountAuth selectUmsAccountAuthById(Long id)
    {
        return umsAccountAuthMapper.selectUmsAccountAuthById(id);
    }

    /**
     * 查询授权信息列表
     * 
     * @param umsAccountAuth 授权信息
     * @return 授权信息
     */
    @Override
    public List<UmsAccountAuth> selectUmsAccountAuthList(UmsAccountAuth umsAccountAuth)
    {
        return umsAccountAuthMapper.selectUmsAccountAuthList(umsAccountAuth);
    }

    /**
     * 新增授权信息
     * 
     * @param umsAccountAuth 授权信息
     * @return 结果
     */
    @Override
    public int insertUmsAccountAuth(UmsAccountAuth umsAccountAuth)
    {
        return umsAccountAuthMapper.insertUmsAccountAuth(umsAccountAuth);
    }

    @Override
    public UmsAccountAuth selectUmsAccountInfoByIdentifier(UmsAccountAuth umsAccountAuth) {
        return umsAccountAuthMapper.selectUmsAccountInfoByIdentifier(umsAccountAuth);
    }

    /**
     * 修改授权信息
     * 
     * @param umsAccountAuth 授权信息
     * @return 结果
     */
    @Override
    public int updateUmsAccountAuth(UmsAccountAuth umsAccountAuth)
    {
        return umsAccountAuthMapper.updateUmsAccountAuth(umsAccountAuth);
    }

    /**
     * 批量删除授权信息
     * 
     * @param ids 需要删除的授权信息ID
     * @return 结果
     */
    @Override
    public int deleteUmsAccountAuthByIds(Long[] ids)
    {
        return umsAccountAuthMapper.deleteUmsAccountAuthByIds(ids);
    }

    /**
     * 删除授权信息信息
     * 
     * @param id 授权信息ID
     * @return 结果
     */
    @Override
    public int deleteUmsAccountAuthById(Long id)
    {
        return umsAccountAuthMapper.deleteUmsAccountAuthById(id);
    }
}
