package com.ruoyi.captcha;/*
 *Copyright © 2018 anji-plus
 *安吉加加信息技术有限公司
 *http://www.anji-plus.com
 *All rights reserved.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UeitCaptchaApplication {
    public static void main(String[] args) {
        SpringApplication.run(UeitCaptchaApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  验证码模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
             "                 _   _                                     _            _             \n" +
                "                (_) | |                                   | |          | |            \n" +
                "  _   _    ___   _  | |_   ______    ___    __ _   _ __   | |_    ___  | |__     __ _ \n" +
                " | | | |  / _ \\ | | | __| |______|  / __|  / _` | | '_ \\  | __|  / __| | '_ \\   / _` |\n" +
                " | |_| | |  __/ | | | |_           | (__  | (_| | | |_) | | |_  | (__  | | | | | (_| |\n" +
                "  \\__,_|  \\___| |_|  \\__|           \\___|  \\__,_| | .__/   \\__|  \\___| |_| |_|  \\__,_|\n" +
                "                                                  | |                                 \n" +
                "                                                  |_|                                 ");
    }
}
