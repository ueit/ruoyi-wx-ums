package com.ueit.vote;

import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableRyFeignClients
@SpringBootApplication
public class UeitVoteApplication {
    public static void main(String[] args){
        SpringApplication.run(UeitVoteApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  投票模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
           "                 _   _                                      _           _                                         _          \n" +
                "                (_) | |                                    | |         | |                                       | |         \n" +
                "  _   _    ___   _  | |_   ______   _ __ ___     ___     __| |  _   _  | |   ___   ___   ______  __   __   ___   | |_    ___ \n" +
                " | | | |  / _ \\ | | | __| |______| | '_ ` _ \\   / _ \\   / _` | | | | | | |  / _ \\ / __| |______| \\ \\ / /  / _ \\  | __|  / _ \\\n" +
                " | |_| | |  __/ | | | |_           | | | | | | | (_) | | (_| | | |_| | | | |  __/ \\__ \\           \\ V /  | (_) | | |_  |  __/\n" +
                "  \\__,_|  \\___| |_|  \\__|          |_| |_| |_|  \\___/   \\__,_|  \\__,_| |_|  \\___| |___/            \\_/    \\___/   \\__|  \\___|\n" +
                "                                                                                                                             \n" +
                "                                                                                                                             ");
    }
}
