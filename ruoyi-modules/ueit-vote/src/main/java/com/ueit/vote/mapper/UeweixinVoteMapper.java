package com.ueit.vote.mapper;

import java.util.List;
import com.ueit.vote.domain.UeweixinVote;

/**
 * 投票活动Mapper接口
 * 
 * @author cuixh
 * @date 2021-03-08
 */
public interface UeweixinVoteMapper 
{
    /**
     * 查询投票活动
     * 
     * @param id 投票活动ID
     * @return 投票活动
     */
    public UeweixinVote selectUeweixinVoteById(Long id);

    public UeweixinVote selectUeweixinVoteByToken(String token);

    /**
     * 查询投票活动列表
     * 
     * @param ueweixinVote 投票活动
     * @return 投票活动集合
     */
    public List<UeweixinVote> selectUeweixinVoteList(UeweixinVote ueweixinVote);

    /**
     * 新增投票活动
     * 
     * @param ueweixinVote 投票活动
     * @return 结果
     */
    public int insertUeweixinVote(UeweixinVote ueweixinVote);

    /**
     * 修改投票活动
     * 
     * @param ueweixinVote 投票活动
     * @return 结果
     */
    public int updateUeweixinVote(UeweixinVote ueweixinVote);

    /**
     * 删除投票活动
     * 
     * @param id 投票活动ID
     * @return 结果
     */
    public int deleteUeweixinVoteById(Long id);

    /**
     * 批量删除投票活动
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUeweixinVoteByIds(Long[] ids);
}
