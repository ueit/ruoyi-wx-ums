package com.ueit.vote.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ueit.vote.mapper.UeweixinVoteRecordMapper;
import com.ueit.vote.domain.UeweixinVoteRecord;
import com.ueit.vote.service.IUeweixinVoteRecordService;

/**
 * 投票记录Service业务层处理
 * 
 * @author cuixh
 * @date 2021-03-08
 */
@Service
public class UeweixinVoteRecordServiceImpl implements IUeweixinVoteRecordService 
{
    @Autowired
    private UeweixinVoteRecordMapper ueweixinVoteRecordMapper;

    /**
     * 查询投票记录
     * 
     * @param id 投票记录ID
     * @return 投票记录
     */
    @Override
    public UeweixinVoteRecord selectUeweixinVoteRecordById(Long id)
    {
        return ueweixinVoteRecordMapper.selectUeweixinVoteRecordById(id);
    }

    /**
     * 查询投票记录列表
     * 
     * @param ueweixinVoteRecord 投票记录
     * @return 投票记录
     */
    @Override
    public List<UeweixinVoteRecord> selectUeweixinVoteRecordList(UeweixinVoteRecord ueweixinVoteRecord)
    {
        return ueweixinVoteRecordMapper.selectUeweixinVoteRecordList(ueweixinVoteRecord);
    }

    /**
     * 新增投票记录
     * 
     * @param ueweixinVoteRecord 投票记录
     * @return 结果
     */
    @Override
    public int insertUeweixinVoteRecord(UeweixinVoteRecord ueweixinVoteRecord)
    {
        return ueweixinVoteRecordMapper.insertUeweixinVoteRecord(ueweixinVoteRecord);
    }

    /**
     * 修改投票记录
     * 
     * @param ueweixinVoteRecord 投票记录
     * @return 结果
     */
    @Override
    public int updateUeweixinVoteRecord(UeweixinVoteRecord ueweixinVoteRecord)
    {
        return ueweixinVoteRecordMapper.updateUeweixinVoteRecord(ueweixinVoteRecord);
    }

    /**
     * 批量删除投票记录
     * 
     * @param ids 需要删除的投票记录ID
     * @return 结果
     */
    @Override
    public int deleteUeweixinVoteRecordByIds(Long[] ids)
    {
        return ueweixinVoteRecordMapper.deleteUeweixinVoteRecordByIds(ids);
    }

    /**
     * 删除投票记录信息
     * 
     * @param id 投票记录ID
     * @return 结果
     */
    @Override
    public int deleteUeweixinVoteRecordById(Long id)
    {
        return ueweixinVoteRecordMapper.deleteUeweixinVoteRecordById(id);
    }

    @Override
    public int CountRecord(UeweixinVoteRecord countRecord) {
        return ueweixinVoteRecordMapper.countRecordByParams(countRecord);
    }

}
