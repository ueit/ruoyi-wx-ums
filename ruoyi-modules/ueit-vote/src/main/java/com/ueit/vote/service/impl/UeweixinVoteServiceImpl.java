package com.ueit.vote.service.impl;

import java.util.List;

import cn.hutool.core.util.RandomUtil;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ueit.vote.mapper.UeweixinVoteMapper;
import com.ueit.vote.domain.UeweixinVote;
import com.ueit.vote.service.IUeweixinVoteService;

/**
 * 投票活动Service业务层处理
 * 
 * @author cuixh
 * @date 2021-03-08
 */
@Service
public class UeweixinVoteServiceImpl implements IUeweixinVoteService 
{
    @Autowired
    private UeweixinVoteMapper ueweixinVoteMapper;

    /**
     * 查询投票活动
     * 
     * @param id 投票活动ID
     * @return 投票活动
     */
    @Override
    public UeweixinVote selectUeweixinVoteById(Long id)
    {
        return ueweixinVoteMapper.selectUeweixinVoteById(id);
    }

    @Override
    public UeweixinVote selectUeweixinVoteByToken(String token) {
        return ueweixinVoteMapper.selectUeweixinVoteByToken(token);
    }

    /**
     * 查询投票活动列表
     * 
     * @param ueweixinVote 投票活动
     * @return 投票活动
     */
    @Override
    public List<UeweixinVote> selectUeweixinVoteList(UeweixinVote ueweixinVote)
    {
        return ueweixinVoteMapper.selectUeweixinVoteList(ueweixinVote);
    }

    /**
     * 新增投票活动
     * 
     * @param ueweixinVote 投票活动
     * @return 结果
     */
    @Override
    public int insertUeweixinVote(UeweixinVote ueweixinVote)
    {
        ueweixinVote.setCreateTime(DateUtils.getNowDate());
        if(StringUtils.isNull(ueweixinVote.getToken())){
            ueweixinVote.setToken( RandomUtil.randomString(16));
        }
        return ueweixinVoteMapper.insertUeweixinVote(ueweixinVote);
    }

    /**
     * 修改投票活动
     * 
     * @param ueweixinVote 投票活动
     * @return 结果
     */
    @Override
    public int updateUeweixinVote(UeweixinVote ueweixinVote)
    {
        if(StringUtils.isNull(ueweixinVote.getToken())){
            ueweixinVote.setToken( RandomUtil.randomString(16));
        }
        return ueweixinVoteMapper.updateUeweixinVote(ueweixinVote);
    }

    /**
     * 批量删除投票活动
     * 
     * @param ids 需要删除的投票活动ID
     * @return 结果
     */
    @Override
    public int deleteUeweixinVoteByIds(Long[] ids)
    {
        return ueweixinVoteMapper.deleteUeweixinVoteByIds(ids);
    }

    /**
     * 删除投票活动信息
     * 
     * @param id 投票活动ID
     * @return 结果
     */
    @Override
    public int deleteUeweixinVoteById(Long id)
    {
        return ueweixinVoteMapper.deleteUeweixinVoteById(id);
    }
}
