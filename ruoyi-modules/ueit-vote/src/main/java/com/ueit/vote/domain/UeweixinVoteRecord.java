package com.ueit.vote.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 投票记录对象 ueweixin_vote_record
 * 
 * @author cuixh
 * @date 2021-03-08
 */
@Data
public class UeweixinVoteRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 投票人id */
    @Excel(name = "投票人id")
    private String accountId;

    /** 投票活动id */
    @Excel(name = "投票活动id")
    private Long voteId;

    /** 投票选项id */
    @Excel(name = "投票选项id")
    private Long voteItemId;

    /** 投票时间戳 */
    @Excel(name = "投票时间戳")
    private Long voteTime;

    /** 投票数 */
    @Excel(name = "投票数")
    private Long voteTouched;

    /** 投票ip地址 */
    @Excel(name = "投票ip地址")
    private String voteIp;

    private Long startTime;
    private Long endTime;
    private String voteToken;


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAccountId(String accountId) 
    {
        this.accountId = accountId;
    }

    public String getAccountId() 
    {
        return accountId;
    }
    public void setVoteId(Long voteId) 
    {
        this.voteId = voteId;
    }

    public Long getVoteId() 
    {
        return voteId;
    }
    public void setVoteItemId(Long voteItemId) 
    {
        this.voteItemId = voteItemId;
    }

    public Long getVoteItemId() 
    {
        return voteItemId;
    }
    public void setVoteTime(Long voteTime) 
    {
        this.voteTime = voteTime;
    }

    public Long getVoteTime() 
    {
        return voteTime;
    }
    public void setVoteTouched(Long voteTouched) 
    {
        this.voteTouched = voteTouched;
    }

    public Long getVoteTouched() 
    {
        return voteTouched;
    }
    public void setVoteIp(String voteIp) 
    {
        this.voteIp = voteIp;
    }

    public String getVoteIp() 
    {
        return voteIp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("accountId", getAccountId())
            .append("voteId", getVoteId())
            .append("voteItemId", getVoteItemId())
            .append("voteTime", getVoteTime())
            .append("voteTouched", getVoteTouched())
            .append("voteIp", getVoteIp())
            .toString();
    }
}
