package com.ueit.vote.mapper;

import java.util.List;
import com.ueit.vote.domain.UeweixinVoteRecord;

/**
 * 投票记录Mapper接口
 * 
 * @author cuixh
 * @date 2021-03-08
 */
public interface UeweixinVoteRecordMapper 
{
    /**
     * 查询投票记录
     * 
     * @param id 投票记录ID
     * @return 投票记录
     */
    public UeweixinVoteRecord selectUeweixinVoteRecordById(Long id);

    /**
     * 查询投票记录列表
     * 
     * @param ueweixinVoteRecord 投票记录
     * @return 投票记录集合
     */
    public List<UeweixinVoteRecord> selectUeweixinVoteRecordList(UeweixinVoteRecord ueweixinVoteRecord);

    /**
     * 新增投票记录
     * 
     * @param ueweixinVoteRecord 投票记录
     * @return 结果
     */
    public int insertUeweixinVoteRecord(UeweixinVoteRecord ueweixinVoteRecord);

    /**
     * 修改投票记录
     * 
     * @param ueweixinVoteRecord 投票记录
     * @return 结果
     */
    public int updateUeweixinVoteRecord(UeweixinVoteRecord ueweixinVoteRecord);

    /**
     * 删除投票记录
     * 
     * @param id 投票记录ID
     * @return 结果
     */
    public int deleteUeweixinVoteRecordById(Long id);

    /**
     * 批量删除投票记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUeweixinVoteRecordByIds(Long[] ids);

    /**
     * 统计数据
     */
    public int countRecordByParams(UeweixinVoteRecord countRecord);
}
