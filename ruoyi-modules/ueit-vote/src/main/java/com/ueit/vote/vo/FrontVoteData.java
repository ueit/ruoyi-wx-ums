package com.ueit.vote.vo;

import lombok.Data;

@Data
public class FrontVoteData {
    private String captchaVerification;
    private String voteId;
    private Long itemId;
}
