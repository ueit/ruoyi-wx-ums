package com.ueit.vote.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ueit.vote.mapper.UeweixinVoteItemMapper;
import com.ueit.vote.domain.UeweixinVoteItem;
import com.ueit.vote.service.IUeweixinVoteItemService;

/**
 * 投票活动选项Service业务层处理
 * 
 * @author cuixh
 * @date 2021-03-08
 */
@Service
public class UeweixinVoteItemServiceImpl implements IUeweixinVoteItemService 
{
    @Autowired
    private UeweixinVoteItemMapper ueweixinVoteItemMapper;

    /**
     * 查询投票活动选项
     * 
     * @param id 投票活动选项ID
     * @return 投票活动选项
     */
    @Override
    public UeweixinVoteItem selectUeweixinVoteItemById(Long id)
    {
        return ueweixinVoteItemMapper.selectUeweixinVoteItemById(id);
    }

    /**
     * 查询投票活动选项列表
     * 
     * @param ueweixinVoteItem 投票活动选项
     * @return 投票活动选项
     */
    @Override
    public List<UeweixinVoteItem> selectUeweixinVoteItemList(UeweixinVoteItem ueweixinVoteItem)
    {
        return ueweixinVoteItemMapper.selectUeweixinVoteItemList(ueweixinVoteItem);
    }

    @Override
    public List<UeweixinVoteItem> selectUeweixinVoteItemListFront(UeweixinVoteItem ueweixinVoteItem) {
        return ueweixinVoteItemMapper.selectUeweixinVoteItemListFront(ueweixinVoteItem);
    }

    /**
     * 新增投票活动选项
     * 
     * @param ueweixinVoteItem 投票活动选项
     * @return 结果
     */
    @Override
    public int insertUeweixinVoteItem(UeweixinVoteItem ueweixinVoteItem)
    {
        return ueweixinVoteItemMapper.insertUeweixinVoteItem(ueweixinVoteItem);
    }

    /**
     * 修改投票活动选项
     * 
     * @param ueweixinVoteItem 投票活动选项
     * @return 结果
     */
    @Override
    public int updateUeweixinVoteItem(UeweixinVoteItem ueweixinVoteItem)
    {
        return ueweixinVoteItemMapper.updateUeweixinVoteItem(ueweixinVoteItem);
    }

    /**
     * 批量删除投票活动选项
     * 
     * @param ids 需要删除的投票活动选项ID
     * @return 结果
     */
    @Override
    public int deleteUeweixinVoteItemByIds(Long[] ids)
    {
        return ueweixinVoteItemMapper.deleteUeweixinVoteItemByIds(ids);
    }

    /**
     * 删除投票活动选项信息
     * 
     * @param id 投票活动选项ID
     * @return 结果
     */
    @Override
    public int deleteUeweixinVoteItemById(Long id)
    {
        return ueweixinVoteItemMapper.deleteUeweixinVoteItemById(id);
    }
}
