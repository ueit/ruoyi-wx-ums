package com.ueit.vote.mapper;

import java.util.List;
import com.ueit.vote.domain.UeweixinVoteItem;

/**
 * 投票活动选项Mapper接口
 * 
 * @author cuixh
 * @date 2021-03-08
 */
public interface UeweixinVoteItemMapper 
{
    /**
     * 查询投票活动选项
     * 
     * @param id 投票活动选项ID
     * @return 投票活动选项
     */
    public UeweixinVoteItem selectUeweixinVoteItemById(Long id);

    /**
     * 查询投票活动选项列表
     * 
     * @param ueweixinVoteItem 投票活动选项
     * @return 投票活动选项集合
     */
    public List<UeweixinVoteItem> selectUeweixinVoteItemList(UeweixinVoteItem ueweixinVoteItem);

    public List<UeweixinVoteItem> selectUeweixinVoteItemListFront(UeweixinVoteItem ueweixinVoteItem);

    /**
     * 新增投票活动选项
     * 
     * @param ueweixinVoteItem 投票活动选项
     * @return 结果
     */
    public int insertUeweixinVoteItem(UeweixinVoteItem ueweixinVoteItem);

    /**
     * 修改投票活动选项
     * 
     * @param ueweixinVoteItem 投票活动选项
     * @return 结果
     */
    public int updateUeweixinVoteItem(UeweixinVoteItem ueweixinVoteItem);

    /**
     * 删除投票活动选项
     * 
     * @param id 投票活动选项ID
     * @return 结果
     */
    public int deleteUeweixinVoteItemById(Long id);

    /**
     * 批量删除投票活动选项
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUeweixinVoteItemByIds(Long[] ids);
}
