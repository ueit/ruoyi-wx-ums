package com.ruoyi.ums.auth;

import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@EnableCustomConfig
@EnableRyFeignClients
@SpringCloudApplication
public class UeitUmsAuthApplication {
    public static void main(String[] args){
        SpringApplication.run(UeitUmsAuthApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  会员认证授权模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
            "                 _   _                                                               _     _     \n" +
                "  _   _    ___  (_) | |_           _   _   _ __ ___    ___            __ _   _   _  | |_  | |__  \n" +
                " | | | |  / _ \\ | | | __|  _____  | | | | | '_ ` _ \\  / __|  _____   / _` | | | | | | __| | '_ \\ \n" +
                " | |_| | |  __/ | | | |_  |_____| | |_| | | | | | | | \\__ \\ |_____| | (_| | | |_| | | |_  | | | |\n" +
                "  \\__,_|  \\___| |_|  \\__|          \\__,_| |_| |_| |_| |___/          \\__,_|  \\__,_|  \\__| |_| |_|\n" +
                "                                                                                                 ");
    }
}
