package com.ruoyi.ums.auth.service;
import cn.hutool.json.JSONObject;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.exception.BaseException;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.system.api.FeignUmsService;
import com.ruoyi.system.api.RemoteLogService;
import com.ruoyi.system.api.domain.UmsAccountAuth;
import com.ruoyi.system.api.model.ThirdAuthUser;
import me.zhyd.oauth.model.AuthUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UmsLoginService {
    private static final Logger log = LoggerFactory.getLogger(UmsLoginService.class);
    @Autowired
    private RemoteLogService remoteLogService;
    @Autowired
    private FeignUmsService feignUmsService;

    public ThirdAuthUser WxRegAndLogin(AuthUser authUser){
        R<ThirdAuthUser> userResult = feignUmsService.getUserInfo(authUser);
        if (R.FAIL == userResult.getCode())
        {
            throw new BaseException(userResult.getMsg());
        }

        if (StringUtils.isNull(userResult) || StringUtils.isNull(userResult.getData()))
        {
            remoteLogService.saveLogininfor(authUser.getUsername(), Constants.LOGIN_FAIL, "登录用户不存在");
            throw new BaseException("登录用户：" + authUser.getUsername() + " 不存在");
        }
        ThirdAuthUser loginUser = userResult.getData();
        remoteLogService.saveLogininfor(authUser.getUsername(), Constants.LOGIN_SUCCESS, "前端用户登录成功");
        return loginUser;
    }
}

