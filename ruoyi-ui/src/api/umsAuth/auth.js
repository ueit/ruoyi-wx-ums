import request from '@/utils/request'

// 查询授权信息列表
export function listAuth(query) {
  return request({
    url: '/ums/auth/list',
    method: 'get',
    params: query
  })
}

// 查询授权信息详细
export function getAuth(id) {
  return request({
    url: '/ums/auth/' + id,
    method: 'get'
  })
}

// 新增授权信息
export function addAuth(data) {
  return request({
    url: '/ums/auth',
    method: 'post',
    data: data
  })
}

// 修改授权信息
export function updateAuth(data) {
  return request({
    url: '/ums/auth',
    method: 'put',
    data: data
  })
}

// 删除授权信息
export function delAuth(id) {
  return request({
    url: '/ums/auth/' + id,
    method: 'delete'
  })
}
