import request from '@/utils/request'

// 查询投票活动列表
export function listVote(query) {
  return request({
    url: '/vote/vote/list',
    method: 'get',
    params: query
  })
}

// 查询投票活动列表作为下拉选使用
export function listVoteToType(query) {
  return request({
    url: '/vote/vote/listToType',
    method: 'get',
    params: query
  })
}

// 查询投票活动详细
export function getVote(id) {
  return request({
    url: '/vote/vote/' + id,
    method: 'get'
  })
}

// 新增投票活动
export function addVote(data) {
  return request({
    url: '/vote/vote',
    method: 'post',
    data: data
  })
}

// 修改投票活动
export function updateVote(data) {
  return request({
    url: '/vote/vote',
    method: 'put',
    data: data
  })
}

// 删除投票活动
export function delVote(id) {
  return request({
    url: '/vote/vote/' + id,
    method: 'delete'
  })
}
