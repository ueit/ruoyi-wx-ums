import request from '@/utils/request'

// 查询会员用户信息列表
export function listUmsInfo(query) {
  return request({
    url: '/ums/umsInfo/list',
    method: 'get',
    params: query
  })
}

// 查询会员用户信息详细
export function getUmsInfo(accountId) {
  return request({
    url: '/ums/umsInfo/' + accountId,
    method: 'get'
  })
}

// 新增会员用户信息
export function addUmsInfo(data) {
  return request({
    url: '/ums/umsInfo',
    method: 'post',
    data: data
  })
}

// 修改会员用户信息
export function updateUmsInfo(data) {
  return request({
    url: '/ums/umsInfo',
    method: 'put',
    data: data
  })
}

// 删除会员用户信息
export function delUmsInfo(accountId) {
  return request({
    url: '/ums/umsInfo/' + accountId,
    method: 'delete'
  })
}
