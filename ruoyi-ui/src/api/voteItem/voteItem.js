import request from '@/utils/request'

// 查询投票活动选项列表
export function listVoteItem(query) {
  return request({
    url: '/vote/voteItem/list',
    method: 'get',
    params: query
  })
}

// 查询投票活动选项详细
export function getVoteItem(id) {
  return request({
    url: '/vote/voteItem/' + id,
    method: 'get'
  })
}

// 新增投票活动选项
export function addVoteItem(data) {
  return request({
    url: '/vote/voteItem',
    method: 'post',
    data: data
  })
}

// 修改投票活动选项
export function updateVoteItem(data) {
  return request({
    url: '/vote/voteItem',
    method: 'put',
    data: data
  })
}

// 删除投票活动选项
export function delVoteItem(id) {
  return request({
    url: '/vote/voteItem/' + id,
    method: 'delete'
  })
}
